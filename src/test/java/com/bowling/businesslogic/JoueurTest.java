package com.bowling.businesslogic;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JoueurTest
{
    private Joueur joueur;

    @org.junit.Before
    public void setUp() throws Exception
    {
        joueur = new Joueur();
    }

    @Test
    public void testJeuAZero() throws Exception
    {
        plusieursLances(0, 20);

        assertEquals(0, joueur.getScore());
    }

    @Test
    public void testJeuAUn() throws Exception
    {
        plusieursLances(1, 20);

        assertEquals(20, joueur.getScore());
    }

    @Test
    public void testSparePuisTrois() throws Exception
    {
        joueur.lancer(5);
        joueur.lancer(5);
        joueur.lancer(3);

        plusieursLances(0, 17);

        assertEquals(16, joueur.getScore());
    }

    @Test
    public void testStrikePuisTroisEtQuatre() throws Exception
    {
        joueur.lancer(10);
        joueur.lancer(3);
        joueur.lancer(4);

        plusieursLances(0, 16);

        assertEquals(24, joueur.getScore());
    }

    @Test
    public void testPartieParfaite() throws Exception
    {
        plusieursLances(10, 12);

        assertEquals(300, joueur.getScore());
    }

    @Test
    public void testPartiedeSpares() throws Exception
    {
        plusieursLances(5, 21);

        assertEquals(150, joueur.getScore());
    }

    private void plusieursLances(int quilles, int nbLances)
    {
        for (int i = 0; i < nbLances; i++) {
            joueur.lancer(quilles);
        }
    }
}
