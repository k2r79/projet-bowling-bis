package com.bowling.businesslogic;

import com.bowling.entity.ScoreEntity;
import com.bowling.entity.ScoreIdEntity;
import com.bowling.exceptions.PseudoExistantException;
import com.bowling.parametres.Parametre;
import org.joda.time.DateTimeUtils;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class PisteTest
{
    private final String JOUEUR_FLORENT = "FloLeOuf";
    private final String JOUEUR_KIM = "KimPossible";
    private final String JOUEUR_VALENTIN = "ValentImpossible";
    private Piste piste;

    @Before
    public void setUp() throws Exception
    {
        piste = new Piste("Piste 1");
        DateTimeUtils.setCurrentMillisFixed(1419505200000L);
    }

    @Test
    public void testAjouterPartie() throws Exception
    {
        Partie partie = new Partie();
        piste.ajouterPartie(partie);

        assertEquals(1, piste.getParties().size());
    }

    @Test
    public void testHeureDisponibleSansPartie() throws Exception
    {
        assertEquals(new LocalDateTime(), piste.getDateDisponible(3));
    }

    @Test
    public void testHeureDisponibleUnePartie() throws Exception
    {
        Partie partie = new Partie(new LocalDateTime(2014, 12, 25, 12, 0, 0, 0));

        ajouterJoueursDeTest(partie);

        piste.ajouterPartie(partie);
        assertEquals(partie.getDateDebut().plusMinutes(3 * Parametre.dureeParJoueur), piste.getDateDisponible(3));
    }

    @Test
    public void testHeureDisponibleDeuxPartiesConsecutives() throws Exception
    {
        Partie partie = new Partie(new LocalDateTime(2014, 12, 25, 12, 0, 0, 0));

        ajouterJoueursDeTest(partie);
        partie.setDateFin(partie.getDateDebut().plusMinutes(partie.getScores().size() * Parametre.dureeParJoueur));

        piste.ajouterPartie(partie);

        Partie partieDeux = new Partie(partie.getDateFin());

        ajouterJoueursDeTest(partieDeux);

        piste.ajouterPartie(partieDeux);
        assertEquals(partie.getDateDebut().plusMinutes(2 * 3 * Parametre.dureeParJoueur), piste.getDateDisponible(3));
    }

    @Test
    public void testHeureDisponibleDeuxPartiesEspacees() throws Exception
    {
        Partie partie = new Partie(new LocalDateTime(2014, 12, 25, 12, 0, 0, 0));

        ajouterJoueursDeTest(partie);
        partie.setDateFin(partie.getDateDebut().plusMinutes(partie.getScores().size() * Parametre.dureeParJoueur));

        piste.ajouterPartie(partie);

        Partie partieDeux = new Partie(new LocalDateTime(2014, 12, 25, 13, 30, 0, 0));

        ajouterJoueursDeTest(partieDeux);

        piste.ajouterPartie(partieDeux);
        assertEquals(partie.getDateDebut().plusMinutes(3 * Parametre.dureeParJoueur), piste.getDateDisponible(3));
    }

    private List<ScoreEntity> ajouterJoueursDeTest(Partie partie) throws PseudoExistantException
    {
        List<ScoreEntity> scores = new ArrayList<ScoreEntity>();

        scores = ajouterJoueur(scores, partie, JOUEUR_FLORENT);
        scores = ajouterJoueur(scores, partie, JOUEUR_KIM);
        scores = ajouterJoueur(scores, partie, JOUEUR_VALENTIN);

        partie.setScores(scores);

        return scores;
    }

    private List<ScoreEntity> ajouterJoueur(List<ScoreEntity> scores, Partie partie, String pseudo)
    {
        Score score = new Score();
        Joueur joueur = new Joueur(pseudo);
        score.setPk(new ScoreIdEntity(joueur.getId(), partie.getId()));
        scores.add(score);

        return scores;
    }
}
