package com.bowling.dao;

import com.bowling.entity.JoueurEntity;
import org.hibernate.ObjectNotFoundException;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JoueurDAOTest extends AbstractDAOTest
{
    public static final String JOUEUR_TEST = "TestPlayer";
    private JoueurDAO joueurDAO;

    @Before
    public void setUp() throws Exception
    {
        joueurDAO = new JoueurDAO();
    }

    @Test
    public void testCRUD() throws Exception
    {
        JoueurEntity joueur = testCreate();
        testFindById(joueur.getId());
        testDelete(joueur);
    }

    private JoueurEntity testCreate() throws Exception
    {
        JoueurEntity joueur = new JoueurEntity(JOUEUR_TEST);

        joueur = joueurDAO.create(joueur);

        assertEquals(joueur.getPseudo(), joueurDAO.findById(joueur.getId()).getPseudo());

        return joueur;
    }

    private void testDelete(JoueurEntity joueur) throws Exception
    {
        joueurDAO.delete(joueur);

        Exception exception = null;
        try {
            joueurDAO.findById(joueur.getId()).getPseudo();
        } catch (Exception e) {
            exception = e;
        }

        assertNotNull(exception);
        assertEquals(ObjectNotFoundException.class, exception.getClass());
    }

    private void testFindById(int id) throws Exception
    {
        assertEquals(JOUEUR_TEST, joueurDAO.findById(id).getPseudo());
    }

    @Test
    public void testFindAll() throws Exception
    {
        List<JoueurEntity> joueurs = joueurDAO.findAll();

        assertEquals(3, joueurs.size());

        assertEquals(JOUEUR_FLORENT, joueurs.get(0).getPseudo());
        assertEquals(JOUEUR_KIM, joueurs.get(1).getPseudo());
        assertEquals(JOUEUR_VALENTIN, joueurs.get(2).getPseudo());
    }
}