package com.bowling.dao;

import com.bowling.util.HibernateUtilTest;
import org.hibernate.Session;
import org.junit.Before;

public abstract class AbstractDAOTest
{
    protected final String JOUEUR_FLORENT = "FloLeOuf";
    protected final String JOUEUR_KIM = "KimPossible";
    protected final String JOUEUR_VALENTIN = "ValentImpossible";

    protected Session session;

    @Before
    public void setUp() throws Exception
    {
        session = HibernateUtilTest.getSessionFactory().getCurrentSession();
    }
}
