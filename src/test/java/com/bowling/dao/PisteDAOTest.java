package com.bowling.dao;

import com.bowling.entity.PisteEntity;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class PisteDAOTest extends AbstractDAOTest
{
    private PisteDAO pisteDAO;

    @Before
    public void setUp() throws Exception
    {
        pisteDAO = new PisteDAO();
    }

    @Test
    public void testFindAll() throws Exception
    {
        List<PisteEntity> pistes = pisteDAO.findAll();

        assertEquals(10, pistes.size());

        int pisteNumber = 1;
        for (PisteEntity piste : pistes) {
            assertEquals("Piste " + pisteNumber, piste.getLibelle());

            pisteNumber++;
        }
    }
}