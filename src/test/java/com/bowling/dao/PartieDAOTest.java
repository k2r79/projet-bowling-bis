package com.bowling.dao;

import com.bowling.entity.JoueurEntity;
import com.bowling.entity.PartieEntity;
import com.bowling.entity.PisteEntity;
import com.bowling.entity.ScoreEntity;
import org.hibernate.ObjectNotFoundException;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedHashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PartieDAOTest extends AbstractDAOTest
{
    private static final LocalDateTime partieDateDebut = new LocalDateTime(2014, 12, 25, 13, 30, 0, 0);
    private PartieDAO partieDAO;
    private PartieEntity partie;

    @Before
    public void setUp() throws Exception
    {
        super.setUp();

        partieDAO = new PartieDAO();
    }

    @Test
    public void testCRUD() throws Exception
    {
        partie = testCreate();
        testFindById(partie.getId());
        testDelete(partie);
    }

    private PartieEntity testCreate() throws Exception
    {
        PartieEntity partie = new PartieEntity(partieDateDebut);
        partie.setPiste((PisteEntity) session.byId(PisteEntity.class).getReference(5));

        partie = partieDAO.create(partie);

        assertEquals(partieDateDebut, partieDAO.findById(partie.getId()).getDateDebut());

        return partie;
    }

    private void testDelete(PartieEntity partie) throws Exception
    {
        partieDAO.delete(partie);

        Exception exception = null;
        try {
            partieDAO.findById(partie.getId()).getDateDebut();
        } catch (Exception e) {
            exception = e;
        }

        assertNotNull(exception);
        assertEquals(ObjectNotFoundException.class, exception.getClass());
    }

    private void testFindById(int id) throws Exception
    {
        assertEquals(partieDateDebut, partieDAO.findById(id).getDateDebut());
    }

    @Test
    public void testGetClassement() throws Exception
    {
        LinkedHashMap<JoueurEntity, ScoreEntity> classement = partieDAO.getClassement(1);

        assertEquals(3, classement.size());
        assertEquals(JOUEUR_FLORENT, ((JoueurEntity) classement.keySet().toArray()[1]).getPseudo());
        assertEquals(JOUEUR_KIM, ((JoueurEntity) classement.keySet().toArray()[2]).getPseudo());
        assertEquals(JOUEUR_VALENTIN, ((JoueurEntity) classement.keySet().toArray()[0]).getPseudo());
    }

    @Test
    public void testGetAllJoueurs() throws Exception
    {
        HashMap<JoueurEntity, ScoreEntity> joueurs = partieDAO.getAllJoueurs((PartieEntity) session.byId(PartieEntity.class).getReference(1));

        assertEquals(3, joueurs.size());
        assertEquals(JOUEUR_FLORENT, ((JoueurEntity) joueurs.keySet().toArray()[0]).getPseudo());
        assertEquals(JOUEUR_KIM, ((JoueurEntity) joueurs.keySet().toArray()[1]).getPseudo());
        assertEquals(JOUEUR_VALENTIN, ((JoueurEntity) joueurs.keySet().toArray()[2]).getPseudo());
    }
}