package com.bowling.dao;

import com.bowling.entity.*;
import org.hibernate.ObjectNotFoundException;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ScoreDAOTest extends AbstractDAOTest
{
    private ScoreDAO scoreDAO;
    private JoueurDAO joueurDAO;
    private PartieDAO partieDAO;
    private PisteDAO pisteDAO;
    private JoueurEntity joueur;
    private PartieEntity partie;
    private PisteEntity piste;

    @Before
    public void setUp() throws Exception
    {
        super.setUp();

        scoreDAO = new ScoreDAO();
    }

    @Test
    public void testCRUD() throws Exception
    {
        InitializeEnvironment();

        ScoreEntity score = testCreate();
        testFindByIdJoueurAndIdPartie(score.getPk().getIdJoueur(), score.getPk().getIdPartie());
        testFindById(new ScoreIdEntity(score.getPk().getIdJoueur(), score.getPk().getIdPartie()));
        testDelete(score);

        PurgeEnvironment();
    }

    private void InitializeEnvironment()
    {
        joueurDAO = new JoueurDAO();
        partieDAO = new PartieDAO();
        pisteDAO = new PisteDAO();

        joueur = new JoueurEntity(JoueurDAOTest.JOUEUR_TEST);
        joueur = joueurDAO.create(joueur);

        piste = new PisteEntity("Test Piste");
        piste = pisteDAO.create(piste);

        partie = new PartieEntity();
        partie.setPiste(piste);
        partie = partieDAO.create(partie);
    }

    private void PurgeEnvironment()
    {
        partieDAO.delete(partie);
        pisteDAO.delete(piste);
        joueurDAO.delete(joueur);
    }

    private ScoreEntity testCreate() throws Exception
    {
        ScoreEntity score = new ScoreEntity(joueur.getId(), partie.getId());
        score = scoreDAO.create(score);

        ScoreEntity persistedScore = scoreDAO.findByIdJoueurAndIdPartie(joueur.getId(), partie.getId());
        assertEquals(score.getPk().getIdJoueur(), persistedScore.getPk().getIdJoueur());
        assertEquals(score.getPk().getIdPartie(), persistedScore.getPk().getIdPartie());
        assertEquals(score.getScore(), persistedScore.getScore());

        return score;
    }

    private void testDelete(ScoreEntity score) throws Exception
    {
        scoreDAO.delete(score);

        Exception exception = null;
        try {
            scoreDAO.findByIdJoueurAndIdPartie(score.getPk().getIdJoueur(), score.getPk().getIdPartie()).getScore();
        } catch (Exception e) {
            exception = e;
        }

        assertNotNull(exception);
        assertEquals(ObjectNotFoundException.class, exception.getClass());
    }

    private void testFindByIdJoueurAndIdPartie(int idJoueur, int idPartie)
    {
        assertEquals(0, scoreDAO.findByIdJoueurAndIdPartie(idJoueur, idPartie).getScore());
    }

    public void testFindById(ScoreIdEntity scoreId) throws Exception
    {
        assertEquals(0, scoreDAO.findById(scoreId).getScore());
    }

    @Test
    public void testFindAll() throws Exception
    {
        List<ScoreEntity> scores = scoreDAO.findAll();

        assertEquals(3, scores.size());

        assertEquals(40, scores.get(0).getScore());
        assertEquals(20, scores.get(1).getScore());
        assertEquals(60, scores.get(2).getScore());
    }
}