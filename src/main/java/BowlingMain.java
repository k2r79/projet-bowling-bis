import com.bowling.GameThread;
import com.bowling.PartieThread;
import com.bowling.entity.PisteEntity;
import com.bowling.socket.Server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class BowlingMain
{
    public static final int port = 1234;

    public static void main(String[] args)
    {
        System.out.println("Serveur lancé...");

        List<PisteEntity> pistes = new ArrayList<PisteEntity>();
        Map<Integer, PartieThread> parties = new HashMap<Integer, PartieThread>();

        GameThread gameThread = new GameThread(pistes, parties);
        new Thread(gameThread).start();

        Thread server = new Thread(new Server(1234, gameThread));
        server.start();
    }
}
