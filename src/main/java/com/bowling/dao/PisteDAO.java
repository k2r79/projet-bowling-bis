package com.bowling.dao;

import com.bowling.entity.PisteEntity;

public class PisteDAO extends AbstractDAO<Integer, PisteEntity>
{
    public PisteDAO()
    {
        super(PisteEntity.class);
    }
}
