package com.bowling.dao;

import com.bowling.entity.JoueurEntity;

public class JoueurDAO extends AbstractDAO<Integer, JoueurEntity>
{
    public JoueurDAO()
    {
        super(JoueurEntity.class);
    }
}