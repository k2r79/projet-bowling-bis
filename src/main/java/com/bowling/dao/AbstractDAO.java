package com.bowling.dao;

import com.bowling.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public abstract class AbstractDAO<PK extends Serializable, T> implements Persistable<PK, T>
{
    protected Session session;
    protected Class<T> classe;

    protected AbstractDAO(Session session, Class<T> classe)
    {
        this.session = session;
        this.classe = classe;
    }

    protected AbstractDAO(Class<T> classe)
    {
        this.classe = classe;
    }

    @Override
    public T create(T object)
    {
        session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            session.beginTransaction();
            session.save(object);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            System.out.println("Erreur de persistance : " + e.getMessage());
            e.printStackTrace();
            session.getTransaction().rollback();
            throw e;
        }

        return object;
    }

    @Override
    public T findById(PK id)
    {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        T object = (T) session.get(classe, id);
        session.getTransaction().commit();

        return object;
    }

    @Override
    public List<T> findAll()
    {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<T> objects = session.createCriteria(classe).list();
        session.getTransaction().commit();

        return objects;
    }

    @Override
    public T update(T object)
    {
        session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            session.beginTransaction();
            session.merge(object);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            System.out.println("Erreur de mise à jour : " + e.getMessage());
            e.printStackTrace();
            session.getTransaction().rollback();
            throw e;
        }

        return object;
    }

    @Override
    public void delete(T object)
    {
        session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            session.beginTransaction();
            session.delete(object);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            System.out.println("Erreur de suppression : " + e.getMessage());
            e.printStackTrace();
            session.getTransaction().rollback();
            throw e;
        }
    }

    @Override
    public void detach(T object)
    {
        session = HibernateUtil.getSessionFactory().getCurrentSession();

        session.beginTransaction();
        session.evict(object);
        session.getTransaction().commit();
    }
}
