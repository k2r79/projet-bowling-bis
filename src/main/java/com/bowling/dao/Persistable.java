package com.bowling.dao;

import java.io.Serializable;
import java.util.List;

public interface Persistable<PK extends Serializable, T>
{
    public T create(T object);

    public T update(T object);

    public void delete(T object);

    public T findById(PK id);

    public List<T> findAll();

    public void detach(T object);
}
