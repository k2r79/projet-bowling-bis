package com.bowling.dao;

import com.bowling.entity.ScoreEntity;
import com.bowling.entity.ScoreIdEntity;
import com.bowling.util.HibernateUtil;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;

public class ScoreDAO extends AbstractDAO<ScoreIdEntity, ScoreEntity>
{
    public ScoreDAO()
    {
        super(ScoreEntity.class);
    }

    public ScoreEntity findByIdJoueurAndIdPartie(int idJoueur, int idPartie)
    {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Query query = session.getNamedQuery("score.findByIdJoueurAndIdPartie")
                .setParameter("idJoueur", idJoueur)
                .setParameter("idPartie", idPartie);

        ScoreEntity score = (ScoreEntity) query.uniqueResult();
        if (score == null) {
            throw new ObjectNotFoundException("ScoreEntity", "Aucun objet trouvé pour idJoueur = " + idJoueur + " & idPartie = " + idPartie);
        }

        session.beginTransaction().commit();

        return score;
    }
}
