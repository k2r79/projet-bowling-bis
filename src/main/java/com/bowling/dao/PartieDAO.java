package com.bowling.dao;

import com.bowling.entity.JoueurEntity;
import com.bowling.entity.PartieEntity;
import com.bowling.entity.ScoreEntity;
import com.bowling.util.HibernateUtil;
import org.hibernate.Query;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class PartieDAO extends AbstractDAO<Integer, PartieEntity>
{
    public PartieDAO()
    {
        super(PartieEntity.class);
    }

    public LinkedHashMap<JoueurEntity, ScoreEntity> getClassement(int idPartie)
    {
        session = HibernateUtil.getSessionFactory().getCurrentSession();

        LinkedHashMap<JoueurEntity, ScoreEntity> classement = new LinkedHashMap<JoueurEntity, ScoreEntity>();

        String queryString = "FROM ScoreEntity AS s, JoueurEntity AS j " +
                       "WHERE s.pk.idPartie = :idPartie " +
                       "    AND s.pk.idJoueur = j.id " +
                       "ORDER BY s.score DESC";

        session.beginTransaction();

        Query query = session.createQuery(queryString)
                             .setParameter("idPartie", idPartie);
        List<Object[]> joueursScores = (List<Object[]>) query.list();

        session.getTransaction().commit();

        for (Object[] queryResults : joueursScores) {
            classement.put((JoueurEntity) queryResults[1], (ScoreEntity) queryResults[0]);
        }

        return classement;
    }

    public HashMap<JoueurEntity, ScoreEntity> getAllJoueurs(PartieEntity partie)
    {
        session = HibernateUtil.getSessionFactory().getCurrentSession();

        HashMap<JoueurEntity, ScoreEntity> joueurs = new HashMap<JoueurEntity, ScoreEntity>();

        String queryString = "FROM JoueurEntity AS j, ScoreEntity AS s " +
                "WHERE s.pk.idPartie = :idPartie " +
                "   AND s.pk.idJoueur = j.id " +
                "ORDER BY j.pseudo";

        session.beginTransaction();

        Query query = session.createQuery(queryString)
                .setParameter("idPartie", partie.getId());
        List<Object[]> joueursScores = (List<Object[]>) query.list();

        session.getTransaction().commit();

        for (Object[] queryResults : joueursScores) {
            joueurs.put((JoueurEntity) queryResults[0], (ScoreEntity) queryResults[1]);
        }



        return joueurs;
    }
}
