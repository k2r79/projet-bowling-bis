package com.bowling.dao;

import com.bowling.entity.ClientEntity;

/**
 * Created by kimsavinfo on 24/06/14.
 */
public class ClientDAO extends AbstractDAO<Integer, ClientEntity>
{
    public ClientDAO() {
        super(ClientEntity.class);
    }
}
