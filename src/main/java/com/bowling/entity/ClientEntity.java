package com.bowling.entity;

import com.bowling.socket.entity.SocketClientEntity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "Joueur")
@DiscriminatorValue(value="C")
public class ClientEntity extends JoueurEntity
{
    @Column(name = "nom", nullable = true)
    private String nom;

    @Column(name = "prenom", nullable = true)
    private String prenom;

    @Column(name = "email", nullable = true)
    private String email;

    @Column(name = "mot_de_passe", nullable = true)
    private String motDePasse;

    @Column(name = "tel", nullable = true)
    private String tel;

    @Column(name = "code_postal", length = 5, nullable = true)
    private String codePostal;

    @Column(name = "token", nullable = true)
    private String token;

    public ClientEntity()
    {
    }

    public ClientEntity(SocketClientEntity socketClientEntity)
    {
        if(socketClientEntity.getId() != -1)
        {
            this.id =  socketClientEntity.getId();
        }
        this.token = socketClientEntity.getToken() != null ? socketClientEntity.getToken() : String.valueOf(UUID.randomUUID());

        this.nom = socketClientEntity.getNom();
        this.prenom = socketClientEntity.getPrenom();
        this.pseudo = socketClientEntity.getPseudo();
        this.motDePasse = socketClientEntity.getMotDePasse();
        this.email = socketClientEntity.getEmail();
        this.tel = socketClientEntity.getTel();
        this.codePostal = socketClientEntity.getCodePostal();
    }

    public ClientEntity(String pseudo)
    {
        super(pseudo);
    }

    public String getNom()
    {
        return nom;
    }
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public String getPrenom()
    {
        return prenom;
    }
    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    public String getEmail()
    {
        return email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getMotDePasse()
    {
        return motDePasse;
    }
    public void setMotDePasse(String motDePasse)
    {
        this.motDePasse = motDePasse;
    }

    public String getTel()
    {
        return tel;
    }
    public void setTel(String tel)
    {
        this.tel = tel;
    }

    public String getCodePostal()
    {
        return codePostal;
    }
    public void setCodePostal(String codePostal)
    {
        this.codePostal = codePostal;
    }

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
}
