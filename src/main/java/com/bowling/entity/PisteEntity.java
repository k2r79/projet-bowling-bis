package com.bowling.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "Piste")
public class PisteEntity implements Serializable
{
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected int id;

    @Column(name = "libelle", unique = true, nullable = false)
    protected String libelle;

    @OneToMany(mappedBy = "piste", fetch = FetchType.EAGER)
    protected List<PartieEntity> parties = new ArrayList<PartieEntity>();

    public PisteEntity(String nom)
    {
        libelle = nom;
        parties = new ArrayList<PartieEntity>();
    }

    public PisteEntity()
    {
    }

    public void ajouterPartie(PartieEntity partie)
    {
        parties.add(partie);
    }

    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public String getLibelle()
    {
        return libelle;
    }
    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public List<PartieEntity> getParties()
    {
        return parties;
    }
    public void setParties(List<PartieEntity> parties)
    {
        this.parties = parties;
    }
}
