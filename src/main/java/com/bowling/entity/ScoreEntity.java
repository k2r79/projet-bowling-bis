package com.bowling.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Score")
@NamedQuery(name = "score.findByIdJoueurAndIdPartie", query = "FROM ScoreEntity WHERE pk.idJoueur = :idJoueur AND pk.idPartie = :idPartie")
public class ScoreEntity implements Serializable
{
    @EmbeddedId
    protected ScoreIdEntity pk;

    @Column(name = "score")
    protected int score;

    public ScoreEntity(int idJoueur, int idPartie)
    {
        pk = new ScoreIdEntity(idJoueur, idPartie);
    }
    public ScoreEntity() {}

    public ScoreIdEntity getPk()
    {
        return pk;
    }
    public void setPk(ScoreIdEntity pk)
    {
        this.pk = pk;
    }

    public int getScore()
    {
        return score;
    }
    public void setScore(int score)
    {
        this.score = score;
    }
}
