package com.bowling.entity;

import com.bowling.dao.PartieDAO;
import com.bowling.socket.entity.SocketPartieEntity;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "Partie")
public class PartieEntity implements Serializable
{
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected int id;

    @Transient
    protected List<ScoreEntity> scores;

    @ManyToOne
    @JoinColumn(name = "id_piste")
    protected PisteEntity piste;

    @Column(name = "date_debut")
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    protected LocalDateTime dateDebut;

    @Column(name = "date_fin")
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    protected LocalDateTime dateFin;

    public PartieEntity(LocalDateTime dateDebut)
    {
        this.dateDebut = dateDebut;
        this.dateFin = null;
    }
    public PartieEntity(SocketPartieEntity socketPartie, PisteEntity piste)
    {
        this.id = socketPartie.getId();
        this.piste = piste;
        this.dateDebut = socketPartie.getDateDebut();
        this.dateFin = socketPartie.getDateFin();
    }
    public PartieEntity()
    {
        this.dateDebut = new LocalDateTime();
        this.dateFin = null;
    }

    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public List<ScoreEntity> getScores()
    {
        return scores;
    }
    public void setScores(List<ScoreEntity> scores)
    {
        this.scores = scores;
    }

    public PisteEntity getPiste()
    {
        return piste;
    }
    public void setPiste(PisteEntity pisteEntity)
    {
        this.piste = pisteEntity;
    }


    public LocalDateTime getDateDebut()
    {
        return dateDebut;
    }
    public void setDateDebut(LocalDateTime dateDebut)
    {
        this.dateDebut = dateDebut;
    }

    public LocalDateTime getDateFin()
    {
        return dateFin;
    }
    public void setDateFin(LocalDateTime dateFin)
    {
        this.dateFin = dateFin;
    }

    public LinkedHashMap<JoueurEntity, ScoreEntity> getClassement()
    {
        PartieDAO partieDAO = new PartieDAO();

        return partieDAO.getClassement(id);
    }
}
