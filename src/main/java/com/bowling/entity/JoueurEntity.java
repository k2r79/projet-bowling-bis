package com.bowling.entity;

import com.bowling.socket.entity.SocketJoueurEntity;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "Joueur", uniqueConstraints=@UniqueConstraint(columnNames = {"pseudo"}))
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "discrimination", discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue("J")
public class JoueurEntity implements Serializable
{
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected int id;

    @Column(name = "pseudo", unique = true, nullable = false)
    protected String pseudo;

    public JoueurEntity() {}
    public JoueurEntity(String pseudo)
    {
        this.pseudo = pseudo;
    }
    public JoueurEntity(SocketJoueurEntity socketJoueur)
    {
        this.id = socketJoueur.getId();
        this.pseudo = socketJoueur.getPseudo();
    }

    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public String getPseudo()
    {
        return pseudo;
    }
    public void setPseudo(String pseudo)
    {
        this.pseudo = pseudo;
    }
}
