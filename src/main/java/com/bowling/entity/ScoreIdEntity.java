package com.bowling.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ScoreIdEntity implements Serializable
{
    @Column(name = "id_partie")
    protected int idPartie;

    @Column(name = "id_joueur")
    protected int idJoueur;

    public ScoreIdEntity(int idJoueur, int idPartie)
    {
        this.idJoueur = idJoueur;
        this.idPartie = idPartie;
    }

    public ScoreIdEntity()
    {
    }

    public int getIdPartie()
    {
        return idPartie;
    }
    public void setIdPartie(int idPartie)
    {
        this.idPartie = idPartie;
    }

    public int getIdJoueur()
    {
        return idJoueur;
    }
    public void setIdJoueur(int idJoueur)
    {
        this.idJoueur = idJoueur;
    }
}
