package com.bowling.parametres;

public class Parametre
{
    public static int nbFrames = 10;
    public static int nbLances = 2;
    public static int nbLancesLastFrame = 3;
    public static int dureeParJoueur = 3; // En minutes
    public static int minimumNumberOfPlayers = 2;
    public static int maximumNumberOfPlayers = 4;
}
