package com.bowling.socket;

import com.bowling.GameThread;
import com.bowling.PartieThread;
import com.bowling.businesslogic.Piste;
import com.bowling.dao.JoueurDAO;
import com.bowling.entity.JoueurEntity;
import com.bowling.entity.PartieEntity;
import com.bowling.entity.PisteEntity;
import com.bowling.entity.ScoreEntity;
import com.bowling.socket.entity.SocketJoueurEntity;
import com.bowling.socket.entity.SocketPartieEntity;
import com.bowling.socket.entity.SocketPisteEntity;
import com.bowling.socket.entity.SocketScoreEntity;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Server implements Runnable
{
    private int port;
    private GameThread gameThread;

    public Server(int port, GameThread gameThread)
    {
        this.port = port;
        this.gameThread = gameThread;
    }

    @Override
    public void run()
    {
        try {
            ServerSocket socket = new ServerSocket(port);
            while (true) {
                SocketThread socketThread = new SocketThread(socket.accept(), this);
                new Thread(socketThread).start();
            }
        } catch (IOException e) {
            System.out.println("Erreur au niveau du serveur de Socket : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public ArrayList<SocketPisteEntity> getPistes()
    {
        JoueurDAO joueurDAO = new JoueurDAO();
        Map<Integer, PartieThread> parties = gameThread.getParties();
        ArrayList<SocketPisteEntity> pistes = new ArrayList<SocketPisteEntity>();

        for (PartieThread partieThread : parties.values()) {
            PisteEntity piste = partieThread.getPiste();

            PartieEntity partie = partieThread.getPartie();
            HashMap<JoueurEntity, ScoreEntity> scores = partieThread.getJoueursScores();

            List<SocketScoreEntity> socketScores = new ArrayList<SocketScoreEntity>();
            for (int indexScore = 0; indexScore < scores.size(); indexScore++) {
                JoueurEntity joueur = (JoueurEntity) scores.keySet().toArray()[indexScore];
                SocketJoueurEntity socketJoueur = new SocketJoueurEntity(joueur.getId(), joueur.getPseudo());
                socketScores.add(new SocketScoreEntity(socketJoueur, scores.get(joueur).getScore()));
            }

            SocketPartieEntity socketPartie = new SocketPartieEntity(partie.getId(), partie.getDateDebut(), new Piste(piste).getDateDisponible(partie.getScores().size()), socketScores);
            SocketPisteEntity socketPiste = new SocketPisteEntity(piste.getId(), piste.getLibelle(), socketPartie);

            pistes.add(socketPiste);
        }

        return pistes;
    }

    public SocketPisteEntity getPiste(int idPiste)
    {
        for (SocketPisteEntity piste : getPistes()) {
            if (piste.getId() == idPiste) {
                return piste;
            }
        }

        return null;
    }
}
