package com.bowling.socket.entity;

import java.io.Serializable;

public class SocketScoreEntity implements Serializable
{
    private SocketJoueurEntity joueur;
    private int score;

    public SocketScoreEntity(SocketJoueurEntity joueur, int score)
    {
        this.joueur = joueur;
        this.score = score;
    }

    public SocketJoueurEntity getJoueur()
    {
        return joueur;
    }
    public void setJoueur(SocketJoueurEntity joueur)
    {
        this.joueur = joueur;
    }

    public int getScore() {
        return score;
    }
    public void setScore(int score) {
        this.score = score;
    }
}
