package com.bowling.socket.entity;

import java.io.Serializable;

public class SocketPisteEntity implements Serializable
{
    private int id;
    private String libelle;
    private SocketPartieEntity partie;

    public SocketPisteEntity(int id, String libelle, SocketPartieEntity partie)
    {
        this.id = id;
        this.libelle = libelle;
        this.partie = partie;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public SocketPartieEntity getPartie()
    {
        return partie;
    }

    public void setPartie(SocketPartieEntity partie)
    {
        this.partie = partie;
    }
}
