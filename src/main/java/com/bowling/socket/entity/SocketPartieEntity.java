package com.bowling.socket.entity;

import org.joda.time.LocalDateTime;

import java.io.Serializable;
import java.util.List;

public class SocketPartieEntity implements Serializable
{
    private int id;
    private LocalDateTime dateDebut;
    private LocalDateTime dateFin;
    private List<SocketScoreEntity> scores;

    public SocketPartieEntity(int id, LocalDateTime dateDebut, LocalDateTime dateFin, List<SocketScoreEntity> scores)
    {
        this.id = id;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.scores = scores;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public LocalDateTime getDateDebut()
    {
        return dateDebut;
    }

    public void setDateDebut(LocalDateTime dateDebut)
    {
        this.dateDebut = dateDebut;
    }

    public LocalDateTime getDateFin()
    {
        return dateFin;
    }

    public void setDateFin(LocalDateTime dateFin)
    {
        this.dateFin = dateFin;
    }

    public List<SocketScoreEntity> getScores()
    {
        return scores;
    }

    public void setScores(List<SocketScoreEntity> scores)
    {
        this.scores = scores;
    }
}
