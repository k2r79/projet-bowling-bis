package com.bowling.socket.entity;

import java.io.Serializable;

/**
 * Created by kimsavinfo on 24/06/14.
 */
public class SocketClientEntity extends SocketJoueurEntity implements Serializable
{
    private String nom;
    private String motDePasse;
    private String prenom;
    private String email;
    private String tel;
    private String codePostal;
    private String token;

    public SocketClientEntity(int id, String pseudo, String motDePasse, String nom, String prenom, String email, String tel, String codePostal)
    {
        super(id, pseudo);
        this.nom = nom;
        this.motDePasse = motDePasse;
        this.prenom = prenom;
        this.email = email;
        this.tel = tel;
        this.codePostal = codePostal;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }
}
