package com.bowling.socket.entity;

import java.io.Serializable;

public class SocketJoueurEntity implements Serializable
{
    protected int id;
    protected String pseudo;

    public SocketJoueurEntity(int id, String pseudo)
    {
        this.id = id;
        this.pseudo = pseudo;
    }

    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public String getPseudo()
    {
        return pseudo;
    }
    public void setPseudo(String pseudo)
    {
        this.pseudo = pseudo;
    }

}
