package com.bowling.socket;

import com.bowling.businesslogic.Piste;
import com.bowling.dao.*;
import com.bowling.entity.*;
import com.bowling.socket.entity.SocketClientEntity;
import com.bowling.socket.entity.SocketPartieEntity;
import com.bowling.socket.entity.SocketScoreEntity;
import org.joda.time.LocalDateTime;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class SocketThread implements Runnable
{
    private Server server;
    private Socket socket;
    private BufferedReader stringIn;
    private PrintWriter stringOut;
    private ClientDAO clientDAO;
    private PisteDAO pisteDAO;
    private PartieDAO partieDAO;
    private JoueurDAO joueurDAO;
    private ScoreDAO scoreDAO;

    public SocketThread(Socket socket, Server server)
    {
        this.socket = socket;
        this.server = server;
        try {
            stringIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            stringOut = new PrintWriter(socket.getOutputStream());
        } catch (IOException e) {
            System.out.println("Erreur d'initialisation d'un élément d'I/O : " + e.getMessage());
            e.printStackTrace();
        }
        this.clientDAO = new ClientDAO();
        this.pisteDAO = new PisteDAO();
        this.partieDAO = new PartieDAO();
        this.joueurDAO = new JoueurDAO();
        this.scoreDAO = new ScoreDAO();
    }

    @Override
    public void run()
    {
        try {
            System.out.println("Client connecté !");

            sendOutputString("Bienvenue !");

            String remoteMessage = "";
            // TODO Set Timeout
            while (!(remoteMessage != null && remoteMessage.equals("exit"))) {
                remoteMessage = readInputString();
                processRemoteMessage(remoteMessage);
            }

            socket.close();
        } catch (IOException e) {
            System.out.println("Une erreur s'est produite au niveau de la socket : " + e.getMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Une erreur s'est produite au niveau de la socket : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void processRemoteMessage(String remoteMessage) throws IOException, ClassNotFoundException
    {
        if (remoteMessage != null) {
            System.out.println("Le client a envoyé : " + remoteMessage);

            if (remoteMessage.equals("bowling.get.pistes")) {
                sendOutputObject(server.getPistes());
            } else if (remoteMessage.contains("bowling.get.piste.")) {
                String[] splittedRemoteMessage = remoteMessage.split("\\.");
                sendOutputObject(server.getPiste(Integer.parseInt(splittedRemoteMessage[splittedRemoteMessage.length - 1])));
            } else if ( remoteMessage.contains("bowling.put.inscription") || remoteMessage.contains("bowling.put.modifierClient") )
            {
                SocketClientEntity socketClientEntity = (SocketClientEntity) readInputObject();
                ClientEntity clientEntity = null;

                if( remoteMessage.equals("bowling.put.inscription") )
                {
                    clientEntity = new ClientEntity(socketClientEntity);
                    clientEntity = clientDAO.create(clientEntity);
                    System.out.println("Nouveau client : " + clientEntity.getNom() + " " + clientEntity.getPrenom() + ", pseudo : " + clientEntity.getPseudo());

                    socketClientEntity.setId(clientEntity.getId());
                    socketClientEntity.setToken(clientEntity.getToken());
                }
                else if(remoteMessage.contains("bowling.put.modifierClient") )
                {
                    clientEntity = new ClientEntity(socketClientEntity);
                    clientDAO.update(clientEntity);

                    System.out.println("Modification du client : " + clientEntity.getId());
                }

                sendOutputObject(socketClientEntity);
            } else if(remoteMessage.contains("bowling.get.reservation.date.")) {
                String[] splittedRemoteMessage = remoteMessage.split("\\.");
                int idPiste = Integer.parseInt(splittedRemoteMessage[splittedRemoteMessage.length - 2]);
                int nbJoueurs = Integer.parseInt(splittedRemoteMessage[splittedRemoteMessage.length - 1]);

                PisteEntity pisteEntity = pisteDAO.findById(idPiste);
                Piste piste = new Piste(pisteEntity);

                sendOutputObject(piste.getDateDisponible(nbJoueurs));
            } else if(remoteMessage.contains("bowling.put.reservation")) {
                String[] splittedRemoteMessage = remoteMessage.split("\\.");
                int idPiste = Integer.parseInt(splittedRemoteMessage[splittedRemoteMessage.length - 1]);

                SocketPartieEntity socketPartie = (SocketPartieEntity) readInputObject();
                PisteEntity pisteEntity = pisteDAO.findById(idPiste);

                socketPartie.setDateDebut(new LocalDateTime());
                PartieEntity partie = partieDAO.create(new PartieEntity(socketPartie, pisteEntity));

                List<ScoreEntity> scores = new ArrayList<ScoreEntity>();
                for (SocketScoreEntity socketScore : socketPartie.getScores()) {
                    JoueurEntity joueur;
                    if (socketScore.getJoueur().getId() == -1) {
                        joueur = joueurDAO.create(new JoueurEntity(socketScore.getJoueur()));
                    } else {
                        joueur = new JoueurEntity(socketScore.getJoueur());
                    }

                    ScoreEntity scoreEntity = new ScoreEntity(joueur.getId(), partie.getId());
                    scoreEntity.setScore(0);
                    scoreEntity = scoreDAO.create(scoreEntity);

                    scores.add(scoreEntity);
                }

                partie.setScores(scores);
                partie = partieDAO.update(partie);

                pisteEntity.ajouterPartie(partie);
                pisteEntity = pisteDAO.update(pisteEntity);
            }
        }
    }

    private String readInputString() throws IOException
    {
        String inputString = stringIn.readLine();

        return inputString;
    }

    private void sendOutputString(String outputString) throws IOException
    {
        stringOut.println(outputString);

        stringOut.flush();
    }

    private void sendOutputObject(Object object) throws IOException
    {
        ObjectOutputStream objectOut = new ObjectOutputStream(socket.getOutputStream());
        objectOut.writeObject(object);

        objectOut.flush();
        socket.getOutputStream().flush();
    }

    public Object readInputObject() throws IOException, ClassNotFoundException
    {
        Object object = null;

        ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
        object = in.readObject();

        return object;
    }
}
