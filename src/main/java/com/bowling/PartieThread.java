package com.bowling;

import com.bowling.businesslogic.Joueur;
import com.bowling.dao.JoueurDAO;
import com.bowling.dao.PartieDAO;
import com.bowling.dao.ScoreDAO;
import com.bowling.entity.JoueurEntity;
import com.bowling.entity.PartieEntity;
import com.bowling.entity.PisteEntity;
import com.bowling.entity.ScoreEntity;
import com.bowling.parametres.Parametre;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import static java.lang.Thread.sleep;

public class PartieThread implements Runnable
{
    private PartieDAO partieDAO;
    private JoueurDAO joueurDAO;
    private ScoreDAO scoreDAO;

    private PartieEntity partie;
    private PisteEntity piste;
    private HashMap<JoueurEntity, ScoreEntity> joueursScores;
    private List<Joueur> joueurs = new ArrayList<Joueur>();
    private List<ScoreEntity> scores = new ArrayList<ScoreEntity>();
    private int numberOfPlayers;

    public PartieThread(PisteEntity piste, int numberOfPlayers)
    {
        partieDAO = new PartieDAO();
        joueurDAO = new JoueurDAO();
        scoreDAO = new ScoreDAO();

        this.piste = piste;
        this.numberOfPlayers = numberOfPlayers;
    }

    @Override
    public void run()
    {
        SetUpPartie();
        RunPartie();
    }

    private void SetUpPartie()
    {
        partie = new PartieEntity(new LocalDateTime());
        partie.setPiste(piste);

        piste.ajouterPartie(partie);
        partie = partieDAO.create(partie);

        List<ScoreEntity> scores = new ArrayList<ScoreEntity>();
        for (int playerNumber = 0; playerNumber < numberOfPlayers; playerNumber++) {
            JoueurEntity joueur = new JoueurEntity();
            joueur.setPseudo("BotPlayer-" + new DateTime().getMillis());

            joueur = joueurDAO.create(joueur);

            ScoreEntity score = new ScoreEntity(joueur.getId(), partie.getId());
            score.setScore(0);

            scoreDAO.create(score);

            scores.add(score);
        }

        partie.setScores(scores);

        joueursScores = partieDAO.getAllJoueurs(partie);
        for (int indexJoueursScores = 0; indexJoueursScores < joueursScores.size(); indexJoueursScores++) {
            this.joueurs.add(new Joueur((JoueurEntity) joueursScores.keySet().toArray()[indexJoueursScores]));
            this.scores.add((ScoreEntity) joueursScores.values().toArray()[indexJoueursScores]);
        }
    }

    private void RunPartie()
    {
        String libellePiste = partie.getPiste().getLibelle();
        int nombreDeLances = (Parametre.nbLances * (Parametre.nbFrames - 1)) + Parametre.nbLancesLastFrame;
        int lancePrecedent = 0;
        for (int numeroLance = 0; numeroLance < nombreDeLances; numeroLance++) {
            for (int indexJoueur = 0; indexJoueur < joueurs.size(); indexJoueur++) {
                Joueur joueur = joueurs.get(indexJoueur);
                ScoreEntity scoreEntity = scores.get(indexJoueur);

                if (joueur.getIndexFrameActuelle() < 10) {
                    int quilles = (int) (Math.random() * (10 - lancePrecedent));
                    System.out.println("[" + libellePiste + "][" + joueur.getPseudo() + "] Lancé : " + quilles);
                    joueur.lancer(quilles);

                    System.out.println("[" + libellePiste + "][" + joueur.getPseudo() + "] Score : " + joueur.getScore());
                    System.out.println();

                    scoreEntity.setScore(joueur.getScore());
                    scoreDAO.update(scoreEntity);

                    joueurs.set(indexJoueur, joueur);
                    scores.set(indexJoueur, scoreEntity);

                } else {
                    System.out.println("[" + libellePiste + "][" + joueur.getPseudo() + "] Partie terminée avec " + joueur.getScore() + " points");
                }

                try {
                    sleep((int) ((float) Parametre.dureeParJoueur / (float) nombreDeLances * 60000.0f));
                } catch (InterruptedException e) {
                    System.out.println("Erreur d'execution de la PartieThread de la piste " + piste.getLibelle());
                    e.printStackTrace();
                    System.exit(3);
                }
            }
        }

        EndPartie(libellePiste);
    }

    private void EndPartie(String libellePiste)
    {
        System.out.println("[" + libellePiste + "] Partie Terminée");
        partie.setDateFin(new LocalDateTime());
        partieDAO.update(partie);

        LinkedHashMap<JoueurEntity, ScoreEntity> classement = partie.getClassement();
        for (int indexClassement = 0; indexClassement < classement.size(); indexClassement++) {
            System.out.println("[" + libellePiste + "] " + (indexClassement + 1) + " : " + ((JoueurEntity) classement.keySet().toArray()[indexClassement]).getPseudo() + " avec " + ((ScoreEntity) classement.values().toArray()[indexClassement]).getScore() + " points");

        }

        System.out.println();
    }

    public PisteEntity getPiste()
    {
        return piste;
    }

    public PartieEntity getPartie()
    {
        return partie;
    }

    public HashMap<JoueurEntity, ScoreEntity> getJoueursScores()
    {
        return joueursScores;
    }
}
