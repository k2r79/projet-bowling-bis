package com.bowling;

import com.bowling.businesslogic.Piste;
import com.bowling.dao.PisteDAO;
import com.bowling.entity.PisteEntity;
import com.bowling.parametres.Parametre;
import org.joda.time.LocalDateTime;

import java.util.List;
import java.util.Map;

import static java.lang.Thread.sleep;

public class GameThread implements Runnable
{
    private List<PisteEntity> pistes;
    private Map<Integer, PartieThread> parties;

    public GameThread(List<PisteEntity> pistes, Map<Integer, PartieThread> parties)
    {
        this.pistes = pistes;
        this.parties = parties;
    }

    @Override
    public void run()
    {
        PisteDAO pisteDAO = new PisteDAO();

        while (true) {
            pistes = pisteDAO.findAll();

            for (PisteEntity pisteEntity : pistes) {
                Piste piste = new Piste(pisteEntity);

                int randomNumberOfPlayers = Parametre.minimumNumberOfPlayers + (int) (Math.random() * Parametre.maximumNumberOfPlayers);

                if (piste.getDateDisponible(randomNumberOfPlayers).compareTo(new LocalDateTime()) <= 0) {

                    try {
                        sleep(5000);
                    } catch (InterruptedException e) {
                        System.out.println("Une erreur d'execution s'est produite : " + e.getMessage());
                        e.printStackTrace();
                        System.exit(2);
                    }

                    System.out.println("Création d'une partie sur la piste : " + piste.getLibelle());
                    PartieThread partie = new PartieThread(piste, randomNumberOfPlayers);

                    System.out.println("Début de la partie sur la piste : " + partie.getPiste().getLibelle());
                    new Thread(partie).start();

                    System.out.println();
                    parties.put(piste.getId(), partie);
                }
            }
        }
    }

    public Map<Integer, PartieThread> getParties()
    {
        return parties;
    }
}
