package com.bowling.exceptions;

public class PseudoExistantException extends Exception
{
    public PseudoExistantException()
    {
        super("Le pseudo que vous avez choisit est déjà attribué");
    }
}
