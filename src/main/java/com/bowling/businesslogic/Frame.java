package com.bowling.businesslogic;

import com.bowling.parametres.Parametre;

public class Frame
{
    int[] lances = new int[Parametre.nbLances];
    private int indexLanceActuel;
    private int score;
    private boolean spare = false;
    private boolean strike = false;
    boolean termine = false;

    public void ajouterLancer(int quilles)
    {
        lances[indexLanceActuel] = quilles;
        score += quilles;

        verifierSpareOuStrike();

        indexLanceActuel++;

        verifierTermine();
    }

    private void verifierSpareOuStrike()
    {
        if (lances[0] + lances[1] == 10) {
            if (getIndexLanceActuel() == 0) {
                strike = true;
            } else {
                spare = true;
            }
        }
    }

    void verifierTermine()
    {
        if (getIndexLanceActuel() == Parametre.nbLances || isStrike() || isSpare()) {
            termine = true;
        }
    }

    public int getIndexLanceActuel()
    {
        return indexLanceActuel;
    }

    public boolean isSpare()
    {
        return spare;
    }

    public boolean isStrike()
    {
        return strike;
    }

    public int[] getLances()
    {
        return lances;
    }

    public int getScore()
    {
        return score;
    }

    public boolean isTermine()
    {
        return termine;
    }

    public void setScore(int score)
    {
        this.score = score;
    }
}
