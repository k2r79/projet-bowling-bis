package com.bowling.businesslogic;

import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.LocalDateTime;

import javax.persistence.Transient;

public class Partie extends com.bowling.entity.PartieEntity
{
    protected Joueur[][] classement;

    public Partie()
    {
        super();
    }
    public Partie(LocalDateTime dateDebut)
    {
        super(dateDebut);
    }

    @Transient
    public int getDuree()
    {
        Interval intervalDuree = new Interval(dateDebut.toDateTime(), dateFin.toDateTime());
        Duration duree = intervalDuree.toDuration();
        return duree.toStandardMinutes().getMinutes();
    }
}
