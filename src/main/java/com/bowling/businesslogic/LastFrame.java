package com.bowling.businesslogic;

import com.bowling.parametres.Parametre;

public class LastFrame extends Frame
{
    public LastFrame()
    {
        super();
        lances = new int[Parametre.nbLancesLastFrame];
    }

    @Override
    protected void verifierTermine()
    {
        if ((getIndexLanceActuel() == Parametre.nbLancesLastFrame && (isStrike() || isSpare()))
                || (getIndexLanceActuel() == Parametre.nbLances && !isStrike() && !isSpare())) {
            termine = true;
        }
    }
}
