package com.bowling.businesslogic;

import com.bowling.entity.JoueurEntity;
import com.bowling.parametres.Parametre;

import javax.persistence.Transient;

public class Joueur extends JoueurEntity
{
    @Transient
    protected Frame[] frames = new Frame[Parametre.nbFrames];

    @Transient
    protected int indexFrameActuelle;

    @Transient
    protected int score;

    public Joueur(String pseudo)
    {
        super(pseudo);
    }

    public Joueur()
    {
        super();
    }

    public Joueur(JoueurEntity joueurEntity)
    {
        this.id = joueurEntity.getId();
        this.pseudo = joueurEntity.getPseudo();
    }

    public Frame[] getFrames()
    {
        return frames;
    }
    public void setFrames(Frame[] frames)
    {
        this.frames = frames;
    }

    public int getIndexFrameActuelle()
    {
        return indexFrameActuelle;
    }
    public void setIndexFrameActuelle(int indexFrameActuelle)
    {
        this.indexFrameActuelle = indexFrameActuelle;
    }

    public int getScore()
    {
        return score;
    }
    public void setScore(int score)
    {
        this.score = score;
    }

    @Transient
    public void lancer(int quilles)
    {
        Frame frameActuelle = frames[indexFrameActuelle];

        if (frameActuelle == null) {
            if (indexFrameActuelle < Parametre.nbFrames - 1) {
                frameActuelle = new Frame();
            } else {
                frameActuelle = new LastFrame();
            }

            frames[indexFrameActuelle] = frameActuelle;
            frameActuelle.setScore(getFramePrecedente().getScore());
        }

        Frame framePrecedente = getFramePrecedente();
        if (framePrecedente.isSpare()) {
            if (frameActuelle.getIndexLanceActuel() == 0) {
                framePrecedente.setScore(framePrecedente.getScore() + quilles);
                frameActuelle.setScore(framePrecedente.getScore());

                frames[indexFrameActuelle - 1] = framePrecedente;
            }
        }

        frameActuelle.ajouterLancer(quilles);

        if (frameActuelle.isTermine()) {
            calculerScoreFrameActuelle(frameActuelle);

            frames[indexFrameActuelle] = frameActuelle;
            score = frameActuelle.getScore();

            indexFrameActuelle++;
        }
    }

    private void calculerScoreFrameActuelle(Frame frameActuelle)
    {
        int indexFramePrecedente;
        if ((indexFramePrecedente = indexFrameActuelle - 1) >= 0) {
            if (frames[indexFramePrecedente].isStrike()) {
                int tmpScore = frameActuelle.getScore() - frames[indexFramePrecedente].getScore();
                frames[indexFramePrecedente].setScore(frameActuelle.getScore());
                frameActuelle.setScore(frameActuelle.getScore() + tmpScore);

                if ((indexFramePrecedente = indexFramePrecedente - 1) >= 0 && indexFrameActuelle != 9) {
                    if (frames[indexFramePrecedente].isStrike()) {
                        frames[indexFramePrecedente].setScore(frames[indexFramePrecedente].getScore() + tmpScore);
                        frames[indexFramePrecedente + 1].setScore(frames[indexFramePrecedente + 1].getScore() + tmpScore);
                        frames[indexFramePrecedente + 2].setScore(frames[indexFramePrecedente + 2].getScore() + tmpScore);
                    }
                }
            }
        }
    }

    @Transient
    public Frame getFramePrecedente()
    {
        return indexFrameActuelle > 0 ? frames[indexFrameActuelle - 1] : frames[indexFrameActuelle];
    }
}
