package com.bowling.businesslogic;

import com.bowling.entity.PartieEntity;
import com.bowling.entity.PisteEntity;
import com.bowling.parametres.Parametre;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;

import javax.persistence.Transient;

public class Piste extends com.bowling.entity.PisteEntity
{
    public Piste(String libelle)
    {
        super(libelle);
    }

    public Piste(PisteEntity piste)
    {
        this.id = piste.getId();
        this.libelle = piste.getLibelle();
        this.parties = piste.getParties();
    }

    @Transient
    public void ajouterPartie(PartieEntity partieEntity)
    {
        parties.add(partieEntity);
    }

    @Transient
    public LocalDateTime getDateDisponible(int nombreJoueurs)
    {
        LocalDateTime dateDisponible = new LocalDateTime();

        if (parties.size() > 1) {
            for (int i = 0 ; i < parties.size() - 1; i++) {
                Duration intervalParties = new Duration(getDateFinEstimee(nombreJoueurs, parties.get(i)).toDateTime(), parties.get(i + 1).getDateDebut().toDateTime());

                if (intervalParties.getStandardMinutes() >= nombreJoueurs * Parametre.dureeParJoueur) {
                    PartieEntity partieEntity = parties.get(i);
                    return getDateFinEstimee(nombreJoueurs, partieEntity);
                }

                if (i == parties.size() - 2) {
                    PartieEntity partieEntity = parties.get(parties.size() - 1);
                    dateDisponible = getDateFinEstimee(nombreJoueurs, partieEntity);
                }
            }
        } else if (parties.size() > 0 ) {
            PartieEntity partieEntity = parties.get(parties.size() - 1);
            dateDisponible = getDateFinEstimee(nombreJoueurs, partieEntity);
        }

        return dateDisponible;
    }

    private LocalDateTime getDateFinEstimee(int nombreJoueurs, PartieEntity partieEntity)
    {
        return partieEntity.getDateFin() != null ? partieEntity.getDateFin() : partieEntity.getDateDebut().plusMinutes(nombreJoueurs * Parametre.dureeParJoueur);
    }
}
